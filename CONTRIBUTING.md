# Contributing to Gravelamps

The developers of Gravelamps have designed Gravelamps to be an extensible platform, to allow new models and frameworks to be added for lensing model selection analyses. If you use Gravelamps and have cause to write a new model, or refine any of the analysis techniques, please consider adding them to Gravelamps using the following procedures and adhering to the code of conduct outlined below.

## Code of Conduct

All people participating in the development of Gravelamps by engaging with the code development, merge requests, issue tracker, etc are expected to treat each other with respect. We would like to make the community using and developing Gravelamps a friendly, inclusive, diverse, and welcoming one for all people.

## Procedures for Contributing

On the whole, if you have an idea for a new feature or change that you would like to see implemented in Gravelamps, the workflow should follow:

- Create your own fork of the repository with the branch containing your change
- Open an issue outlining the change/feature you are planning to add to Gravelamps
- Hack away implementing the change/feature
- When ready, open a merge request for your branch
- One of the developers will initiate code review for your MR, using your issue to keep in contact and request any changes
- Once code review is complete and the feature is approved, your MR will be merged and your change will be added to the next release of Gravelamps

## Style Guide

The developers of Gravelamps try to make sure that the code within Gravelamps is both functional and easily readable. To do so, we follow PEP8 conventions in all python code and we adhere to Google's C++ Style Guide. To ensure that these are followed, we will use the `pylint` and `cpplint` linting programs. 
