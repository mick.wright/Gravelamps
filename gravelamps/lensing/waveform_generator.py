"""Gravelamps Waveform Generators

Following contains class definitions for Gravelamps waveform generators. These are child classes of
the standard bilby WaveformGenerator class.

Written by Mick Wright
           Isaac C. F. Wong 2022

Classes
-------
LensedWaveformGenerator
    Standard total lensed waveform generator used throughout Gravelamps
"""

import importlib

import numpy as np

from bilby.core.utils import infer_parameters_from_function
from bilby.gw.waveform_generator import WaveformGenerator

from gravelamps.core.conversion import (
    frequency_to_dimensionless_frequency,
    lens_mass_to_redshifted_lens_mass,
    solar_mass_to_natural_mass
)
from gravelamps.core.gravelog import gravelogger

class SingleImageGenerator(WaveformGenerator):
    '''
    Single Image of Strong Lensing Waveform Generator class

    Based upon the general bilby WaveformGenerator class, this waveform generator will seek
    a lensing module from the waveform arguments as well as an indication of the image to generate
    and will construct the amplification factor accordingly when calculating the strain
    '''

    def __init__(self, duration=None, sampling_frequency=None, start_time=0,
                 frequency_domain_source_model=None, time_domain_source_model=None,
                 parameters=None, parameter_conversion=None, waveform_arguments=None):

        super().__init__(duration, sampling_frequency, start_time, frequency_domain_source_model,
                         time_domain_source_model, parameters, parameter_conversion,
                         waveform_arguments)

        if "use_redshifted_lens_mass" in waveform_arguments:
            self.redshifted_lens_mass = bool(waveform_arguments["use_redshifted_lens_mass"])
        else:
            self.redshifted_lens_mass = False

        self.lens_module = importlib.import_module(waveform_arguments["lens_module"])
        self.source_parameter_keys.update(self.lens_parameters())

        if hasattr(self.lens_module, "set_scaling"):
            scaling_setter_func = getattr(self.lens_module, "set_scaling")
            scaling_setter_func(waveform_arguments["scaling_constant"])

        self.amplification_factor_func = getattr(self.lens_module, "single_image_amplification")

    def _strain_from_model(self, model_data_points, model):
        unlensed_waveform = model(model_data_points, **self.parameters)

        nat_mass = solar_mass_to_natural_mass(
            self.parameters["redshifted_lens_mass"]
        )

        dimensionless_frequency_array =\
            frequency_to_dimensionless_frequency(model_data_points,
                                                 nat_mass)
        amplification_factor = self.amplification_factor_func(dimensionless_frequency_array,
                                                              self.parameters["source_position"],
                                                              self.waveform_arguments["image"])

        lensed_waveform = {}
        for key, value in unlensed_waveform.items():
            lensed_waveform[key] = np.multiply(
                value, np.conjugate(amplification_factor)
            )
        return lensed_waveform

    @property
    def lens_parameters(self):
        '''
        Lens Parameters to Infer
        '''

        return self._lens_parameters

    def _lens_parameters(self):
        if hasattr(self.lens_module, "_lens_parameters"):
            lens_parameters = self.lens_module._lens_parameters
        elif hasattr(self.lens_module, "get_lens_parameters"):
            parameter_func = getattr(self.lens_module, "get_lens_parameters")
            lens_parameters = parameter_func(self.waveform_arguments)
        else:
            lens_parameters = infer_parameters_from_function(self.amplification_factor_func)

        if self.redshifted_lens_mass:
            lens_parameters =\
                [parameter.replace("lens_mass", "redshifted_lens_mass")\
                    for parameter in lens_parameters if parameter !=
                 "lens_fractional_distance"]

        return lens_parameters

class LensedWaveformGenerator(WaveformGenerator):
    """
    Standard total lensed waveform generator used throughout Gravelamps

    This waveform generator will seek a lensing module from the waveform arguments from which to
    extract the amplification factor calculation function it will then use during calculation of
    the strain data.

    Attributes
    ----------
    lens_module : ModuleType
        The module used to extract lensing amplification factor function from
    lens_parameters : dict
        Additional lens model parameters and their values

    """
    def __init__(self, duration=None, sampling_frequency=None, start_time=0,
                 frequency_domain_source_model=None, time_domain_source_model=None,
                 parameters=None, parameter_conversion=None, waveform_arguments=None):

        #Perform main initialisation from parent class
        super().__init__(duration, sampling_frequency, start_time, frequency_domain_source_model,
                         time_domain_source_model, parameters, parameter_conversion,
                         waveform_arguments)

        if "use_redshifted_lens_mass" in waveform_arguments:
            self.redshifted_lens_mass = bool(waveform_arguments["use_redshifted_lens_mass"])
        else:
            self.redshifted_lens_mass = False

        self.lens_module = importlib.import_module(waveform_arguments["lens_module"])
        self.source_parameter_keys.update(self.lens_parameters())

        if hasattr(self.lens_module, "generate_interpolator"):
            self.lens_module.generate_interpolator(waveform_arguments["dimensionless_frequency"],
                                                   waveform_arguments["source_position_file"],
                                                   waveform_arguments["amplification_factor_real"],
                                                   waveform_arguments["amplification_factor_imag"])

        if hasattr(self.lens_module, "load_table"):
            load_table_func = getattr(self.lens_module, "load_table")
            load_table_func(waveform_arguments["lookup_table_location"])

        if hasattr(self.lens_module, "set_scaling"):
            scaling_setter_func = getattr(self.lens_module, "set_scaling")
            scaling_setter_func(waveform_arguments["scaling_constant"])

        if hasattr(self.lens_module, "amplification_factor"):
            self.amplification_factor_func = getattr(self.lens_module, "amplification_factor")
        else:
            self.amplification_factor_func = None
            gravelogger.warning("No Amplification Factor Function detected, \
                                 signal will be unlensed")

    def _strain_from_model(self, model_data_points, model):
        unlensed_waveform = model(model_data_points, **self.parameters)

        if self.amplification_factor_func is None:
            return unlensed_waveform

        if "lens_mass" in self.source_parameter_keys:
            redshifted_lens_mass =\
                lens_mass_to_redshifted_lens_mass(self.parameters["lens_mass"],
                                                  self.parameters["lens_fractional_distance"],
                                                  self.parameters["luminosity_distance"])
            dimensionless_frequency_array =\
                frequency_to_dimensionless_frequency(model_data_points, redshifted_lens_mass)
            amplification_factor =\
                self.amplification_factor_func(dimensionless_frequency_array,
                                               self.parameters["source_position"])

        elif "redshifted_lens_mass" in self.source_parameter_keys:
            nat_mass = solar_mass_to_natural_mass(
                self.parameters["redshifted_lens_mass"]
            )

            dimensionless_frequency_array =\
                frequency_to_dimensionless_frequency(model_data_points,
                                                     nat_mass)
            amplification_factor =\
                self.amplification_factor_func(dimensionless_frequency_array,
                                               self.parameters["source_position"])

        elif "k" in self.source_parameter_keys:
            image_times, luminosity_distances, phases =\
                self.lens_module.gather_parameter_lists(self.lens_parameters(), self.parameters)

            amplification_factor =\
                self.amplification_factor_func(model_data_points,
                                               int(self.parameters["k"]),
                                               image_times,
                                               luminosity_distances,
                                               phases)

        else:
            amplification_factor = self.amplification_factor_func(model_data_points,
                                                                  **self.parameters)

        lensed_waveform = {}
        for key, value in unlensed_waveform.items():
            lensed_waveform[key] = np.multiply(
                value, np.conjugate(amplification_factor)
            )
        return lensed_waveform

    @property
    def lens_parameters(self):
        """
        Additional lens model parameters and their value

        Returns
        -------
        dict
            Contains the parameters as keys and the associated values

        """

        return self._lens_parameters

    def _lens_parameters(self):
        if hasattr(self.lens_module, "_lens_parameters"):
            lens_parameters = self.lens_module._lens_parameters
        elif hasattr(self.lens_module, "get_lens_parameters"):
            parameter_func = getattr(self.lens_module, "get_lens_parameters")
            lens_parameters = parameter_func(self.waveform_arguments)
        else:
            lens_parameters = infer_parameters_from_function(self.amplification_factor_func)

        if self.redshifted_lens_mass:
            lens_parameters =\
                [parameter.replace("lens_mass", "redshifted_lens_mass")
                    for parameter in lens_parameters if parameter !=
                 "lens_fractional_distance"]

        return lens_parameters
